import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: ` 
    <ngx-spinner
      bdColor="rgba(51,51,51,0.8)"
      size="medium"
      color="#fff"
      type="ball-spin-clockwise">
    </ngx-spinner>
    <notifier-container></notifier-container>
    <app-header></app-header>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
// 
