
import { HeaderComponent } from './header'
import { NavMenuComponent } from './nav-menu'

export const AppComponents = [
    HeaderComponent,
    NavMenuComponent
]
