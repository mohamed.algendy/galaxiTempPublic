import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Router } from "./app.routing";

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { environment } from '../environments/environment';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NotifierModule } from 'angular-notifier'

import { 
  EnumsService,
  ClientsService
} from './services/firebase'

import { HeaderComponent } from './components/header';
import { NavMenuComponent } from './components/nav-menu'
import { AppComponent } from './app.component';
import { LoginPageComponent } from './application/login-page';
import { RegisterPageComponent } from './application/register-page';
import { ApplicationComponent } from './application';
import { ApplicationModule } from './application/application.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RegisterPageComponent,
    ApplicationComponent,
    HeaderComponent,
    NavMenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgxSpinnerModule,
    NotifierModule.withConfig( {
      position: {
        horizontal: {
          position: 'right'
        }
      }, 
      theme: 'material',
      behaviour: {
        autoHide: 2000,
        showDismissButton: false
      }
    } ),
    Router,
    ApplicationModule
  ],
  providers: [
    EnumsService,
    ClientsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
