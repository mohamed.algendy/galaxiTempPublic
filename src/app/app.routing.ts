import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component'
import { LoginPageComponent } from './application/login-page';
import { RegisterPageComponent } from './application/register-page';
import { ApplicationComponent } from './application';
export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    }, {
        path: 'login',
        component: LoginPageComponent
    }, {
        path: 'register',
        component: RegisterPageComponent
    }, {
        path: 'application',
        component: ApplicationComponent
    }
];
  
// , {
//     path: 'login',
//     component: LoginPageComponent
// }, {
//     path: 'register',
//     component: RegisterPageComponent
// }, {
//     path: 'application',
//     loadChildren: 'app/application/'
// }

  export const Router = RouterModule.forRoot(appRoutes);
  