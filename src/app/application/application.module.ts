import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home';
import { DaysListComponent } from './days-list'
import { Router } from './application.routing';
import { ManageEnumsComponent } from './manage-enums/manage-enums.component';
import { UniversitiesListComponent } from './universities-list';
import { FaculitiesListComponent } from './faculities-list';
import { StudentActivitiesListComponent } from './student-activities-list';
import { KnowAboutUsListComponent } from './know-about-us-list';
import { UniversityCreateEditComponent } from './university-create-edit';
import { FaculityCreateEditComponent } from './faculity-create-edit';
import { StudentActivityCreateEditComponent } from './studentactivity-create-edit';
import { KnowusCreateEditComponent } from './knowus-create-edit';
import { ClientsComponent } from './clients/clients.component';
import { DaySheetComponent } from './day-sheet/day-sheet.component';
import { ClientCreateEditComponent } from './client-create-edit/client-create-edit.component';

@NgModule({
  declarations: [
     HomeComponent,
     DaysListComponent,
     ManageEnumsComponent,
     UniversitiesListComponent,
     FaculitiesListComponent,
     StudentActivitiesListComponent,
     KnowAboutUsListComponent,
     UniversityCreateEditComponent,
     FaculityCreateEditComponent,
     StudentActivityCreateEditComponent,
     KnowusCreateEditComponent,
     ClientsComponent,
     DaySheetComponent,
     ClientCreateEditComponent
  ],
  entryComponents:[
    UniversityCreateEditComponent,
    FaculityCreateEditComponent,
    StudentActivityCreateEditComponent,
    KnowusCreateEditComponent,
    ClientCreateEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    Router
  ],
  providers: [],
})
export class ApplicationModule { }
