import { Component, OnInit } from '@angular/core';
import { EnumsService } from '../../services/firebase';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier'

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { UniversityCreateEditComponent } from '../university-create-edit';

@Component({
  selector: 'app-universities-list',
  templateUrl: './universities-list.component.html',
  styleUrls: ['./universities-list.component.css']
})
export class UniversitiesListComponent implements OnInit {

  // Reference variables
  listItems = [];
  componentService = this.enumsService.universities;
  filterString;
  listSubscription;
  // Actions
  list = function(){
    var _self = this;
    if(_self.listSubscription){
      _self.listSubscription.unsubscribe();
    }
    _self.spinner.show();
    _self.listSubscription = _self.componentService.list(_self.filterString).subscribe(
      function(res){
        _self.listItems = res;
        _self.spinner.hide();
        console.log("_self.listItems", _self.listItems);
      }, function(err){
        console.log("error");
        _self.spinner.hide();
      }, function(){
        _self.spinner.hide();
      }
    );
  }
  add = function(name){
    var _self = this;
    _self.spinner.show();
    _self.componentService.add(name).then(
      res => _self.notifierService.notify( 'success', 'Added successfully!' ),
      err => _self.notifierService.notify( 'error', 'Error: not Added!' )
    ).finally(function(){
      _self.spinner.hide();
    });
  }
  edit = function(item){
    var _self = this;
    _self.spinner.show();
    _self.componentService.update(item).then(
      res => _self.notifierService.notify( 'success', 'Edited successfully!' ),
      err => _self.notifierService.notify( 'error', 'Error: not Edited!' )  
    ).finally(function(){
        _self.spinner.hide();
      });
  }
  delete = function(item){
    var _self = this;
    if(!confirm("Are you sure you want to delete "+item.name))
      return;
    _self.spinner.show();
    _self.componentService.delete(item).then(
      res => _self.notifierService.notify( 'success', 'Deleted successfully!' ),
      err => _self.notifierService.notify( 'error', 'Error: not Deleted!' )
    ).finally(function(){
      _self.spinner.hide();
    });
  }
  openAddModal = function(){
    var _self = this;
    const modalRef = this.modalService.open(UniversityCreateEditComponent);
    modalRef.result.then(
      function(data){
        var item = data;
        _self.add(item.name);
      }, function(){}
    );
  }
  openEditModal = function(item){
    var _self = this;
    const modalRef = this.modalService.open(UniversityCreateEditComponent);
    modalRef.componentInstance.item = item;
    modalRef.result.then(
      function(data){
        var item = data;
        _self.edit(item);
      }, function(){}
    );
  }

  // Contructor
  constructor(
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    private modalService: NgbModal,
    private enumsService: EnumsService) {
  }

  // Life hooks
  ngOnInit() {
    var _self = this;
    _self.list();
  }

}
