import { Component, OnInit } from '@angular/core';
import { EnumsService } from '../../services/firebase';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FaculityCreateEditComponent } from '../faculity-create-edit';

@Component({
  selector: 'app-faculities-list',
  templateUrl: './faculities-list.component.html',
  styleUrls: ['./faculities-list.component.css']
})
export class FaculitiesListComponent implements OnInit {

  // Reference variables
  listItems = [];
  componentService = this.enumsService.faculities;
  filterString;
  listSubscription;
  // Actions
  list = function(){
    var _self = this;
    if(_self.listSubscription){
      _self.listSubscription.unsubscribe();
    }
    _self.spinner.show();
    _self.listSubscription = _self.componentService.list(_self.filterString).subscribe(
      function(res){
        _self.listItems = res;
        _self.spinner.hide();
        console.log("_self.listItems", _self.listItems);
      }, function(err){
        console.log("error");
        _self.spinner.hide();
      }, function(){
        _self.spinner.hide();
      }
    );
  }
  add = function(name){
    var _self = this;
    _self.spinner.show();
    _self.componentService.add(name).then(
      function(res){
    }, function(err){

    }).finally(function(){
      _self.spinner.hide();
    });
  }
  edit = function(item){
    var _self = this;
    _self.spinner.show();
    _self.enumsService.universities.update(item).then(
      function(res){
      }, function(err){

      }).finally(function(){
        _self.spinner.hide();
      });
  }
  delete = function(item){
    var _self = this;
    _self.spinner.show();
    _self.componentService.delete(item).then(
      function(){
      }, function(){

      }
    ).finally(function(){
      _self.spinner.hide();
    });
  }
  openAddModal = function(){
    var _self = this;
    const modalRef = this.modalService.open(FaculityCreateEditComponent);
    modalRef.result.then(
      function(data){
        var item = data;
        _self.add(item.name);
      }, function(){}
    );
  }
  openEditModal = function(item){
    var _self = this;
    const modalRef = this.modalService.open(FaculityCreateEditComponent);
    modalRef.componentInstance.item = item;
    modalRef.result.then(
      function(data){
        var item = data;
        _self.edit(item);
      }, function(){}
    );
  }

  // Contructor
  constructor(
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private enumsService: EnumsService) {
  }

  // Life hooks
  ngOnInit() {
    var _self = this;
    _self.list();
  }

}
