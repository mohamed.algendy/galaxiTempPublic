import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-studentactivity-create-edit',
  templateUrl: './studentactivity-create-edit.component.html',
  styleUrls: ['./studentactivity-create-edit.component.css']
})
export class StudentActivityCreateEditComponent implements OnInit {
 
  // Reference variables
  @Input() item;
  // Actions
  save= function(){
    var _self = this;
    this.activeModal.close(_self.item);
  }
  close = function(){
    var _self = this;
    this.activeModal.dismiss("Dismissed");
  }

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    var _self = this;
    _self.item = _self.item || {};
  }

}
