import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-faculity-create-edit',
  templateUrl: './faculity-create-edit.component.html',
  styleUrls: ['./faculity-create-edit.component.css']
})
export class FaculityCreateEditComponent implements OnInit {
 
  // Reference variables
  @Input() item;
  // Actions
  save= function(){
    var _self = this;
    this.activeModal.close(_self.item);
  }
  close = function(){
    var _self = this;
    this.activeModal.dismiss("Dismissed");
  }
  // Additional
  autoFocus(){
    document.getElementById("faculity-name-input").focus();
  }

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    var _self = this;
    _self.item = _self.item || {};
    _self.autoFocus();
  }

}
