import { Component } from '@angular/core';

// import { NavMenuComponent } from '../components/nav-menu';
// import { HeaderComponent } from '../components/header';

@Component({
  selector: 'app-application',
  template: `
    <app-nav-menu></app-nav-menu>
    <div class="main-page-content">
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent {

}