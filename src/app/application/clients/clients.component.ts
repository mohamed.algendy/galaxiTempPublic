import { Component, OnInit } from '@angular/core';
import { ClientsService, EnumsService } from '../../services/firebase';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ClientCreateEditComponent } from '../client-create-edit';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  // Reference variables
  listItems = [];
  universitiesList;
  faculitiesList;
  studentActivitiesList;
  filterString;
  listSubscription;
  // Actions
  list = function(){
    var _self = this;
    if(_self.listSubscription){
      _self.listSubscription.unsubscribe();
    }
    _self.spinner.show();
    _self.listSubscription = _self.componentService.list(_self.filterString).subscribe(
      function(res){
        _self.listItems = res;
        _self.spinner.hide();
        console.log("_self.listItems", _self.listItems);
      }, function(err){
        console.log("error");
        _self.spinner.hide();
      }, function(){
        _self.spinner.hide();
      }
    );
  }
  delete = function(item){
    if(!confirm("Are you sure you want to delete client "+item.name))
      return;
    var _self = this;
    _self.spinner.show();
    _self.componentService.delete(item).then(
      res => _self.notifierService.notify( 'success', 'Deleted successfully!' ),
      err => _self.notifierService.notify( 'error', 'Error: not Deleted!' )
    ).finally(function(){
      _self.spinner.hide();
    });
  }
  openAddModal = function(){
    var _self = this;
    const modalRef = this.modalService.open(ClientCreateEditComponent);
    modalRef.result.then(
      function(data){
      }, function(){}
    );
  }
  openEditModal = function(id){
    var _self = this;
    const modalRef = this.modalService.open(ClientCreateEditComponent);
    modalRef.componentInstance.resolve = {
      clientId: id
    };
    modalRef.result.then(
      function(data){
      }, function(){}
    );
  }
  getEnums = function(){
    var _self = this;
    _self.spinner.show();
    Observable.forkJoin([
      _self.enumsService.universities.list().first(),
      _self.enumsService.faculities.list().first(),
      _self.enumsService.studentActivities.list().first()
    ]).subscribe(res => {
      _self.universitiesList = res[0];
      _self.faculitiesList = res[1];
      _self.studentActivitiesList = res[2];
      _self.spinner.hide();
      _self.list();
    }, err => {
      console.log("err", err);
    });
  }
  getUniversityOf(id){
    var _self = this;
    if(!id)
      return "-";
    var obj = _self.universitiesList.find(x=>x.id==id);
    if(!obj)
      return "removed";
    return obj.name;
  }
  getFacultyOf(id){
    var _self = this;if(!id)
      return "";
    var obj = _self.faculitiesList.find(x=>x.id==id);
    if(!obj)
      return "removed";
    return obj.name;
  }
  getStudentActivityOf(id){
    var _self = this;if(!id)
      return "";
    var obj = _self.studentActivitiesList.find(x=>x.id==id);
    if(!obj)
      return "removed";
    return obj.name;
  }
  // Contructor
  constructor(
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    private modalService: NgbModal,
    private componentService: ClientsService,
    private enumsService: EnumsService) {
  }

  // Life hooks
  ngOnInit() {
    var _self = this;
    _self.getEnums();
  }

}
