import { Component, OnInit, Input } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { EnumsService } from '../../services/firebase/enums.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotifierService } from 'angular-notifier'  
import { ClientsService } from '../../services/firebase/clients.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';

@Component({
  selector: 'app-client-create-edit',
  templateUrl: './client-create-edit.component.html',
  styleUrls: ['./client-create-edit.component.css']
})
export class ClientCreateEditComponent implements OnInit {

  // Reference variables
  @Input() resolve;
  isEdit;
  clientId;
  client : any = {};
  universitiesList;
  faculitiesList;
  knowUsList;
  studentActivitiesList;
  academicYearList = [1, 2, 3, 4, 5, "Graduated"];
  selectedStudentActivityId = null;
  modalLoading = false;

  // Actions
  save = function(){
    var _self = this;
    if(!_self.isEdit) {
      _self.client.dateCreated = new Date();
      _self.client.dateUpdated = new Date();
      _self.client.numberOfVisits = 0;
      _self.client.hoursSpent = 0;
      _self.client.coworkingPayments = 0;
      _self.client.kitchenPayments = 0;
      _self.client.totalPayments = 0;
      _self.componentService.add(_self.client).then( 
        res => _self.notifierService.notify( 'success', 'Added successfully!' ),
        err => _self.notifierService.notify( 'error', 'Error: not added'+'!' )
      );
    } else {
      _self.client.id = _self.clientId;
      _self.componentService.update(_self.client).then( 
        res => _self.notifierService.notify( 'success', 'Edited successfully!' ),
        err => _self.notifierService.notify( 'error', 'Error: not edited!' )
      );
    }
    this.activeModal.close(_self.client);
  }
  close = function(){
    var _self = this;
    this.activeModal.dismiss("Dismissed");
  }
  getEnums = function(){
    var _self = this;
    _self.modalLoading = true;
    Observable.forkJoin([
      _self.enumsService.universities.list().first(),
      _self.enumsService.faculities.list().first(),
      _self.enumsService.knowAboutUs.list().first(),
      _self.enumsService.studentActivities.list().first()
    ]).subscribe(res => {
      _self.universitiesList = res[0];
      _self.faculitiesList = res[1];
      _self.knowUsList = res[2];
      _self.studentActivitiesList = res[3];
      _self.modalLoading = false;
      if(_self.isEdit){
        _self.getClient(_self.clientId);
      }
    }, err => {
      console.log("err", err);
    });
  }
  getClient(id){
    var _self = this;
    console.log("id", id);
    _self.componentService.get(id).subscribe(
      res=> _self.client = res,
      err=> console.log(err)
    );
  }
  addStudentActivity(){
    var _self = this;
    if(!_self.selectedStudentActivityId) 
      return;
    if(!_self.client.studentActivitiesIds){
      _self.client.studentActivitiesIds = [];
    }
    // Check if already exist
    for(var i=0;i<_self.client.studentActivitiesIds.length;i++){
      if(_self.client.studentActivitiesIds[i]==_self.selectedStudentActivityId) {
        _self.selectedStudentActivityId = null;
        return;
      }
    }
    _self.client.studentActivitiesIds.push(_self.selectedStudentActivityId);
    _self.selectedStudentActivityId = null;
  }
  removeStudentActivity(id){
    var _self = this;
    var index = _self.client.studentActivitiesIds.indexOf(id);
    if (index > -1) {
      _self.client.studentActivitiesIds.splice(index, 1);
    }
  }
  getNameOfId(id){
    var _self = this;
    return _self.studentActivitiesList.find(sa=>sa.id==id).name;
  }

  constructor(
    private spinner: NgxSpinnerService,
    private notifierService: NotifierService,
    public activeModal: NgbActiveModal,
    private enumsService: EnumsService,
    private componentService: ClientsService
  ) { 

  }

  ngOnInit() {
    var _self = this;
    _self.isEdit = _self.resolve && _self.resolve.clientId ? true : false;
    console.log("_self.isEdit",_self.isEdit);
    _self.resolve = _self.resolve || {};
    _self.clientId = _self.resolve.clientId || null;
    _self.getEnums();
    
  }

}
