import { Component, OnInit } from '@angular/core';
import { EnumsService } from '../../services/firebase';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { StudentActivityCreateEditComponent } from '../studentactivity-create-edit'

@Component({
  selector: 'app-student-activities-list',
  templateUrl: './student-activities-list.component.html',
  styleUrls: ['./student-activities-list.component.css']
})
export class StudentActivitiesListComponent implements OnInit {

  // Reference variables
  listItems = [];
  componentService = this.enumsService.studentActivities;
  filterString;
  listSubscription;
  // Actions
  list = function(){
    var _self = this;
    if(_self.listSubscription){
      _self.listSubscription.unsubscribe();
    }
    _self.spinner.show();
    _self.listSubscription = _self.componentService.list(_self.filterString).subscribe(
      function(res){
        _self.listItems = res;
        _self.spinner.hide();
        console.log("_self.listItems", _self.listItems);
      }, function(err){
        console.log("error");
        _self.spinner.hide();
      }, function(){
        _self.spinner.hide();
      }
    );
  }
  add = function(name){
    var _self = this;
    _self.spinner.show();
    _self.componentService.add(name).then(
      function(res){

    }, function(err){

    }).finally(function(){
      _self.spinner.hide();
    });
  }
  edit = function(item){
    var _self = this;
    _self.spinner.show();
    _self.componentService.update(item).then(
      function(res){

      }, function(err){

      }).finally(function(){
        _self.spinner.hide();
      });
  }
  delete = function(item){
    var _self = this;
    _self.spinner.show();
    _self.componentService.delete(item).then(
      function(){

      }, function(){

      }
    ).finally(function(){
      _self.spinner.hide();
    });
  }
  openAddModal = function(){
    var _self = this;
    const modalRef = this.modalService.open(StudentActivityCreateEditComponent);
    modalRef.result.then(
      function(data){
        _self.add(data);
      }, function(){}
    );
  }
  openEditModal = function(item){
    var _self = this;
    const modalRef = this.modalService.open(StudentActivityCreateEditComponent);
    modalRef.componentInstance.item = item;
    modalRef.result.then(
      function(data){
        _self.edit(data);
      }, function(){}
    );
  }

  // Contructor
  constructor(
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private enumsService: EnumsService) {
  }

  // Life hooks
  ngOnInit() {
    var _self = this;
    _self.list();
  }
}
