import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-enums',
  templateUrl: './manage-enums.component.html',
  styleUrls: ['./manage-enums.component.css']
})
export class ManageEnumsComponent implements OnInit {

  // Reference variables
  selectedEnum = "Universities";
  enumsList = [
    "Universities",
    "Faculities",
    "Student Activities",
    "Know about us"
  ];

  constructor() {
   }

  ngOnInit() {
  }

}
