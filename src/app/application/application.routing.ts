import { RouterModule, Routes } from '@angular/router';

import { ApplicationComponent } from './application.component';
import { HomeComponent } from './home';
import { DaysListComponent } from './days-list';
import { ManageEnumsComponent } from './manage-enums';
import { ClientsComponent } from './clients';
const appRoutes: Routes = [
    {
        path: 'application',
        component: ApplicationComponent,
        children: [
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'home',
                component: HomeComponent
            }, {
                path: 'days/list',
                component: DaysListComponent
            }, {
                path: 'manageenums',
                component: ManageEnumsComponent
            }, {
                path: 'clients',
                component: ClientsComponent
            }
        ]
    }
];


  

export const Router = RouterModule.forRoot(appRoutes);
  