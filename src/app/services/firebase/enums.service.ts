import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/map';

@Injectable()
export class EnumsService {


    // Reference variables
    universities: {};
    faculities: {};
    studentActivities: {};
    knowAboutUs: {};
    private itemDoc: AngularFirestoreDocument<any>;

    constructor( private db: AngularFirestore ) {
        //db.firestore.settings({ timestampsInSnapshots: true });

        var self = this;

        // Universities service
        this.universities = {
            collectionName: 'universities',
            searchingField: 'name',
            list: function(queryString){
                queryString = queryString || "";
                var collection = self.db.collection(this.collectionName, 
                    ref => ref.orderBy(this.searchingField).startAt(queryString).endAt(queryString+"\uf8ff"));
                return collection.snapshotChanges().map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data();
                      const id = a.payload.doc.id;
                      return { id, ...data };
                    });
                  });;
            },
            add: function(name){
                var collection = self.db.collection(this.collectionName);
                const item = { name: name};
                return collection.add(item);
            },
            update: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.update(item);
            },
            delete: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.delete();
            }
        }
        // end of universities service

        // Faculities Service
        this.faculities = {
            collectionName: 'faculities',
            searchingField: 'name',
            list: function(queryString){
                queryString = queryString || "";
                var collection = self.db.collection(this.collectionName, 
                    ref => ref.orderBy(this.searchingField).startAt(queryString).endAt(queryString+"\uf8ff"));
                return collection.snapshotChanges().map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data();
                      const id = a.payload.doc.id;
                      return { id, ...data };
                    });
                  });;
            },
            add: function(name){
                var collection = self.db.collection(this.collectionName);
                const item = { name: name};
                return collection.add(item);
            },
            update: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.update(item);
            },
            delete: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.delete();
            }
        }
        // end of faculities service

        // Student activities Service
        this.studentActivities = {
            collectionName: 'studentactivities',
            searchingField: 'name',
            list: function(queryString){
                queryString = queryString || "";
                var collection = self.db.collection(this.collectionName, 
                    ref => ref.orderBy(this.searchingField).startAt(queryString).endAt(queryString+"\uf8ff"));
                return collection.snapshotChanges().map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data();
                      const id = a.payload.doc.id;
                      return { id, ...data };
                    });
                  });;
            },
            add: function(item){
                var collection = self.db.collection(this.collectionName);
                return collection.add(item);
            },
            update: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.update(item);
            },
            delete: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.delete();
            }
        }
        // end of student activities service

        // know about us Service
        this.knowAboutUs = {
            collectionName: 'knowaboutus',
            searchingField: 'name',
            list: function(queryString){
                queryString = queryString || "";
                var collection = self.db.collection(this.collectionName, 
                    ref => ref.orderBy(this.searchingField).startAt(queryString).endAt(queryString+"\uf8ff"));
                return collection.snapshotChanges().map(actions => {
                    return actions.map(a => {
                      const data = a.payload.doc.data();
                      const id = a.payload.doc.id;
                      return { id, ...data };
                    });
                  });;
            },
            add: function(item){
                var collection = self.db.collection(this.collectionName);
                return collection.add(item);
            },
            update: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.update(item);
            },
            delete: function(item){
                self.itemDoc = self.db.doc(this.collectionName+'/'+item.id);
                return self.itemDoc.delete();
            }
        }
        // end of know about us Service service


     }

}
