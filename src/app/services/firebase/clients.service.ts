import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import 'rxjs/add/operator/map';

@Injectable()
export class ClientsService {


    // Reference variables
    collectionName = "clients";
    private itemDoc: AngularFirestoreDocument<any>;


    list(queryString) {
        var _self = this;
        queryString = queryString || "";
        var searchingField = "phone";
        var collection = _self.db.collection(_self.collectionName, 
            ref => ref.orderBy(searchingField).startAt(queryString).endAt(queryString+"\uf8ff"));
        return collection.snapshotChanges().map(actions => {
            return actions.map(a => {
                const data = a.payload.doc.data();
                const id = a.payload.doc.id;
                return { id, ...data };
            });
        });
    }
    add(item){
        var _self = this;
        var collection = _self.db.collection(_self.collectionName);
        return collection.add(item);
    }
    update(item){
        var _self = this;
        _self.itemDoc = _self.db.doc(_self.collectionName+'/'+item.id);
        return _self.itemDoc.update(item);
    }
    delete(item){
        var _self = this;
        _self.itemDoc = _self.db.doc(_self.collectionName+'/'+item.id);
        return _self.itemDoc.delete();
    }
    get(id){
        var _self = this;
        _self.itemDoc = _self.db.doc(_self.collectionName+'/'+id)
        return _self.itemDoc.valueChanges();
    }

    // Contructor
    constructor( private db: AngularFirestore ){}
}
