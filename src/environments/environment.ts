// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBVzsNBWkd_ZdhX5zrHvQC6A0FpzM4xGtc",
    authDomain: "galaxicoworkingspace-33325.firebaseapp.com",
    databaseURL: "https://galaxicoworkingspace-33325.firebaseio.com",
    projectId: "galaxicoworkingspace-33325",
    storageBucket: "galaxicoworkingspace-33325.appspot.com",
    messagingSenderId: "428608865823",
  }
};
